version: '3'
services:
  docs:
    image: ${docker_repository}stingar/stingar-user-docs:latest
  elasticsearch:
    image: ${docker_repository}stingar/elasticsearch:latest
    volumes:
      - es_data:/usr/share/elasticsearch/data:z
    ports:
      - "127.0.0.1:9200:9200"
      - "127.0.0.1:9300:9300"
    environment:
      discovery.type: "single-node"
      ES_JAVA_OPTS: "-Xmx256m -Xms256m"
  kibana:
    image: ${docker_repository}stingar/kibana:latest
    ports:
      - "127.0.0.1:5601:5601"
    depends_on:
      - elasticsearch
  fluentd:
    image: ${docker_repository}stingar/fluentd:latest
    ports:
      - "24224:24224"
      - "24224:24224/udp"
      - "127.0.0.1:24225:24225"
      - "127.0.0.1:24225:24225/udp"
    env_file:
      - stingar.env
    depends_on:
      - elasticsearch
  stingarapi:
    image: ${docker_repository}stingar/stingar-api:latest
    env_file:
      - stingar.env
    volumes:
      - ./storage/db:/srv/db:z
    depends_on:
      - elasticsearch
  stingarui:
    image: ${docker_repository}stingar/stingar-ui:latest
    env_file:
      - ./stingar.env
    volumes:
      - bundle:/bundle
      - node_module_cache:/stingar-ui/node_modules
  web:
    image: nginx
    container_name: nginx
    ports:
      - 80:80
      - 443:443
    volumes:
      - ${cert_path}:/etc/nginx/conf.d:z
      - ./nginx.conf:/etc/nginx/nginx.conf:ro
      - credentials:/credentials
    restart: on-failure
  langstroth:
    image: ${docker_repository}stingar/langstroth:latest
    env_file:
      - stingar.env
    depends_on:
      - stingarapi
volumes:
  bundle:
  node_module_cache:
  credentials:
  es_data: